declare interface IUser {
    readonly email: string
    password: string
}

declare interface IRegistrationUser {
    readonly firstName?: string
    readonly lastName?: string
    readonly fullName?: string
    readonly email: string
    password: string
    readonly passwordAgain?: string
    readonly phone: string
    readonly helpPhone?: string
    readonly company?: string
    readonly businessName: string
    readonly websiteUrl?: string
    readonly website?: string
    readonly country: 'US' | 'CA'
    readonly zip?: string
    readonly city?: string
    readonly state?: string
    readonly timezone?: string
    readonly street1?: string
    readonly street2?: string
}

type MemberType = 'member' | 'manager'
