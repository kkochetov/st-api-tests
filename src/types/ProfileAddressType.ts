export class ProfileAddress {
    public city = ''
    public country = 'US'
    public state = ''
    public street1 = ''
    public street2 = ''
    public timezone = 'US/Eastern'
    public zip = ''
}
