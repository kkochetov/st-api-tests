export enum AnalyticFieldTypes {
    FULL_NAME = 'full-name',
    PHONE = 'phone-number',
    COMPANY = 'company-name',
    WEBSITE = 'website',
}
