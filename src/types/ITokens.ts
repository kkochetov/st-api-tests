declare interface ITokens {
    csrf: string
    remember: string
}
