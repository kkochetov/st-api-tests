class UserData {
    public email?: string
    public fullName: string | undefined
    public phone: string | undefined
    public country: string | undefined
    public websiteUrl = ''
    public businessName = ''
}
