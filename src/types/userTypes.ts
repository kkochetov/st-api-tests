export enum UserType {
    ALL_LISTS = 'all_lists',
    ACCOUNT_ANALYTICS = 'account_analytics',
    ACCOUNT_PROFILE_ANALYTICS = 'account_profile_analytics',
    BILLING_PAID_ANALYTICS = 'billing_paid_analytics',
}
