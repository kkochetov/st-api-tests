import { google } from 'googleapis'
import * as googleServiceAccountKey from '../../res/dev-google-service.json'
import { BASE_URL } from '../config/globalConfig'
import request from 'supertest'

export const getAdminTokens = async (accessToken = 'service'): Promise<ITokens> => {
    if (accessToken === 'service') {
        accessToken = await getGoogleAccessToken()
    }
    return (await getAdminTokenRequest(accessToken)) || ''
}

const getAdminTokenRequest = async (accessToken: string): Promise<ITokens> => {
    let token = ''
    const req = await request(getAdminRestUrl()).get(`/admin-rest/auth/google/token?accessToken=${accessToken}`)
    token = req.body.authToken
    const csrfToken = await getAdminCsrfToken(token)
    return { csrf: csrfToken, remember: token }
}

export const getAdminCsrfToken = async (adminAuthToken: string) => {
    const res = await request(getAdminRestUrl())
        .put('/admin-rest/api/csrf-token')
        .set('cookie', `st-admin-auth=${adminAuthToken};`)
    return res.body.token
}

const getGoogleAccessToken = async () => {
    const googleJWTClient = new google.auth.JWT(
        googleServiceAccountKey.client_email,
        undefined,
        googleServiceAccountKey.private_key,
        ['https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'],
        undefined,
    )
    const finalToken = (await googleJWTClient.authorize()).access_token as string
    return finalToken
}

function getAdminRestUrl(): any {
    return BASE_URL.replace('://', '://admin-')
}
