import { utcToZonedTime } from 'date-fns-tz'

export const getZonedTime = async (offset: number) => {
    const newDate = new Date(Date.now() + offset * 1000)
    const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone
    return utcToZonedTime(newDate, timeZone).toISOString()
}

export const getZonedDate = async (days: number) => {
    const newDate = new Date()
    newDate.setDate(newDate.getDate() + days)
    return newDate.toISOString().slice(0, 10)
}
