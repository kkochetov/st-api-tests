// Logic to handle Cookies and CSRF with 3 options:
export function analyzeAndFormatAuthData(cookies?: string[], authTokens?: ITokens, headers?: any, isAdmin = false) {
    let finalData: string[] = []
    const cookieNameToken = isAdmin ? 'st-admin-auth' : 'remember-me'
    if (cookies && cookies.length) {
        // 1. cookies arr contains valid data => add remember-me cookie value
        const rememberValIndex = cookies.findIndex((e) => e.includes(cookieNameToken))
        if (authTokens && rememberValIndex !== -1) {
            cookies[rememberValIndex] = cookieNameToken + `=${authTokens.remember}`
        }
        finalData = cookies
    } else if (!cookies) {
        // 2. cookies arr not passed => set remember-me if it was passed in reqData
        finalData.push(`${cookieNameToken}=${authTokens?.remember}`)
    } else {
        // 3. cookies arr empty => cookies should be empty. Remember token not used even from reqData
        finalData = []
    }
    headers = headers || {}
    if (authTokens) {
        headers['X-CSRF-Token'] = authTokens.csrf
    }
    return { finalCookies: finalData, finalHeaders: headers }
}
