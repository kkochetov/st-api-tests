import jp from 'jsonpath'
import WaitForResult from '../../core/waitForResult'
import { newRegistrationUser } from '../../data/users.data'
import { AdminRequests } from '../../requests/adminRequests'
import { AnalyticRequests } from '../../requests/analyticRequests'

describe('Ban/Unban account', () => {
    let usernameId: string
    const user = newRegistrationUser.ACCOUNT_ANALYTICS

    beforeAll(async () => {
        jest.setTimeout(20000)

        const res = await AdminRequests.registerUser(user, true)
        expect(res.statusCode).toEqual(200)

        const userRes = await AdminRequests.getUserByEmail(user.email)
        usernameId = jp.query(userRes.body, '$.content[0].tenantId')[0]
    })

    afterAll(async () => {
        await AdminRequests.deleteUser(usernameId)
    })

    it('Ban account', async () => {
        const res = await AdminRequests.banAccount(usernameId)
        expect(res.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[0].type')[0]).toEqual('ACCOUNT_BANNED')
        }).Run()
    })

    it('Unban account', async () => {
        const res = await AdminRequests.unbanAccount(usernameId)
        expect(res.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[0].type')[0]).toEqual('ACCOUNT_UNBANNED')
        }).Run()
    })

    // TODO: Need info about this disables test
    // it('Account - Assigned Phone Update', async () => {
    //     const r = await updatePhoneNumber(authData, '4984949822')
    //     expect(r.statusCode).toEqual(200)

    //     await new WaitForResult(async () => {
    //         const getAnalyticResp = await tenantHapsGet(usernameId, 7)
    //         expect(getAnalyticResp.statusCode).toEqual(200)

    //         const body = getAnalyticResp.body
    //         console.log(body)
    //         expect(jp.query(body, '$.content[0].find{it.type == "ACCOUNT_ASSIGNED_PHONE_UPDATE"}.tenantId')[0]).toEqual(
    //             usernameId,
    //         )
    //     }).Run()
    // })
})
