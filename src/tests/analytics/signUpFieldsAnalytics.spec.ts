import { newRegistrationUser } from '../../data/users.data'
import { Constants } from '../../data/constants'
import jp from 'jsonpath'
import WaitForResult from '../../core/waitForResult'
import { AnalyticFieldTypes } from '../../types/AnalyticFieldTypes'
import { AdminRequests } from '../../requests/adminRequests'
import { AnalyticRequests } from '../../requests/analyticRequests'
import { UserRequests } from '../../requests/userRequests'

describe('Analytic Sign Up Fields', () => {
    let usernameId: string
    const user = newRegistrationUser.ALL_LISTS
    const discount = false
    const secondsBefore = 80
    let authData: ITokens

    const analyticaFieldsData = [
        { valid: 'true', error: '' },
        { valid: 'false', error: 'error1' },
    ]

    beforeAll(async () => {
        jest.setTimeout(20000)

        const signUpResp = await UserRequests.signUpUser(user.email, user.password, discount, Constants.TEST_TOKEN)
        expect(signUpResp.statusCode).toEqual(200)
        usernameId = signUpResp.body.username

        authData = await UserRequests.getAuthData(user.email, user.password)
    })

    afterAll(async () => {
        await AdminRequests.deleteUser(usernameId)
    })

    for (const e of analyticaFieldsData) {
        it(`Filling Full Name Field with state '${e.valid}' and error '${e.error}'`, async () => {
            const sendAnalyticsResp = await AnalyticRequests.sendAnalyticsField(
                authData,
                AnalyticFieldTypes.FULL_NAME,
                e,
            )
            expect(sendAnalyticsResp.statusCode).toEqual(201)

            await new WaitForResult(async () => {
                const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, secondsBefore)
                expect(getAnalyticResp.statusCode).toEqual(200)

                const body = getAnalyticResp.body
                expect(jp.query(body, '$.content[?(@.type=="SIGN_UP_FULL_NAME_FILLED")].tenantId')[0]).toEqual(
                    usernameId,
                )
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_FULL_NAME_FILLED")].values.fullNameIsValid')[0],
                ).toEqual(e.valid)
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_FULL_NAME_FILLED")].values.fullNameError')[0],
                ).toEqual(e.error)
            }).Run()
        })
    }

    for (const e of analyticaFieldsData) {
        it(`Filling Phone Number Field with state '${e.valid}' and error '${e.error}'`, async () => {
            const sendAnalyticsResp = await AnalyticRequests.sendAnalyticsField(authData, AnalyticFieldTypes.PHONE, e)
            expect(sendAnalyticsResp.statusCode).toEqual(201)

            await new WaitForResult(async () => {
                const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, secondsBefore)
                expect(getAnalyticResp.statusCode).toEqual(200)

                const body = getAnalyticResp.body

                expect(jp.query(body, '$.content[?(@.type=="SIGN_UP_PHONE_NUMBER_FILLED")].tenantId')[0]).toEqual(
                    usernameId,
                )
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_PHONE_NUMBER_FILLED")].values.phoneNumberIsValid')[0],
                ).toEqual(e.valid)
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_PHONE_NUMBER_FILLED")].values.phoneNumberError')[0],
                ).toEqual(e.error)
            }).Run()
        })
    }

    for (const e of analyticaFieldsData) {
        it(`Filling Company Name Field with state '${e.valid}' and error '${e.error}'`, async () => {
            const sendAnalyticsResp = await AnalyticRequests.sendAnalyticsField(authData, AnalyticFieldTypes.COMPANY, e)
            expect(sendAnalyticsResp.statusCode).toEqual(201)

            await new WaitForResult(async () => {
                const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, secondsBefore)
                expect(getAnalyticResp.statusCode).toEqual(200)

                const body = getAnalyticResp.body

                expect(jp.query(body, '$.content[?(@.type=="SIGN_UP_ORGANIZATION_FILLED")].tenantId')[0]).toEqual(
                    usernameId,
                )
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_ORGANIZATION_FILLED")].values.companyNameIsValid')[0],
                ).toEqual(e.valid)
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_ORGANIZATION_FILLED")].values.companyNameError')[0],
                ).toEqual(e.error)
            }).Run()
        })
    }

    for (const e of analyticaFieldsData) {
        it(`Filling Website Field with state '${e.valid}' and error '${e.error}'`, async () => {
            const sendAnalyticsResp = await AnalyticRequests.sendAnalyticsField(authData, AnalyticFieldTypes.WEBSITE, e)
            expect(sendAnalyticsResp.statusCode).toEqual(201)

            await new WaitForResult(async () => {
                const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, secondsBefore)
                expect(getAnalyticResp.statusCode).toEqual(200)

                const body = getAnalyticResp.body

                expect(jp.query(body, '$.content[?(@.type=="SIGN_UP_WEBSITE_FILLED")].tenantId')[0]).toEqual(usernameId)
                expect(
                    jp.query(body, '$.content[?(@.type=="SIGN_UP_WEBSITE_FILLED")].values.websiteIsValid')[0],
                ).toEqual(e.valid)
                expect(jp.query(body, '$.content[?(@.type=="SIGN_UP_WEBSITE_FILLED")].values.websiteError')[0]).toEqual(
                    e.error,
                )
            }).Run()
        })
    }
})
