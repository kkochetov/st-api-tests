import jp from 'jsonpath'
import WaitForResult from '../../core/waitForResult'
import { ProfileInfo } from '../../types/ProfileInfoType'
import { ProfileAddress } from '../../types/ProfileAddressType'
import { AdminRequests } from '../../requests/adminRequests'
import { AnalyticRequests } from '../../requests/analyticRequests'
import { UserRequests } from '../../requests/userRequests'
import { newRegistrationUser } from '../../data/users.data'

describe('Account Profile/Owner Info Updated', () => {
    let usernameId: string
    let authData: ITokens
    const user = newRegistrationUser.ACCOUNT_PROFILE_ANALYTICS

    beforeAll(async () => {
        jest.setTimeout(20000)

        const r = await AdminRequests.registerUser(user, true)
        expect(r.statusCode).toEqual(200)

        const userRes = await AdminRequests.getUserByEmail(user.email)
        usernameId = jp.query(userRes.body, '$.content[0].tenantId')[0]

        authData = await UserRequests.getAuthData(user.email, user.password)
    })

    afterAll(async () => {
        await AdminRequests.deleteUser(usernameId)
    })

    it('Global Account Info Updated', async () => {
        const newProfileInfo = new ProfileInfo()
        newProfileInfo.websiteUrl = 'testing.com'

        const generalInfo = {
            changeGeneralInfo: true,
            generalInfo: newProfileInfo,
        }

        const updateProfileInfoResp = await UserRequests.updateProfileInfo(authData, generalInfo)
        expect(updateProfileInfoResp.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="ACCOUNT_DETAILS_UPDATED")].tenantId')[0]).toEqual(usernameId)
        }).Run()
    })

    // TODO: ask about this disabled test
    // it('Email Change Request', async () => {})

    it('Country updated', async () => {
        const newProfileAddress = new ProfileAddress()
        newProfileAddress.country = 'CA'

        const changeAddressInfo = {
            changeAddress: true,
            address: newProfileAddress,
        }

        const updateProfileInfoResp = await UserRequests.updateProfileInfo(authData, changeAddressInfo)
        expect(updateProfileInfoResp.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="ACCOUNT_COUNTRY_UPDATED")].tenantId')[0]).toEqual(usernameId)
        }).Run()
    })

    it('Company renamed', async () => {
        const newProfileInfo = new ProfileInfo()
        newProfileInfo.company = 'TEST_THAT'

        const generalInfo = {
            changeGeneralInfo: true,
            generalInfo: newProfileInfo,
        }

        const updateProfileInfoResp = await UserRequests.updateProfileInfo(authData, generalInfo)
        expect(updateProfileInfoResp.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="ACCOUNT_DETAILS_UPDATED")].tenantId')[0]).toEqual(usernameId)
        }).Run()
    })
})
