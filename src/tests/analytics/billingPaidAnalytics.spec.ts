import jp from 'jsonpath'
import WaitForResult from '../../core/waitForResult'
import { newRegistrationUser } from '../../data/users.data'
import { getZonedDate } from '../../helpers/dateHelpers'
import { AdminRequests } from '../../requests/adminRequests'
import { AnalyticRequests } from '../../requests/analyticRequests'
import { UserRequests } from '../../requests/userRequests'
describe('Paid Billing', () => {
    let usernameId: string
    let authData: ITokens
    const user = newRegistrationUser.BILLING_PAID_ANALYTICS
    beforeAll(async () => {
        jest.setTimeout(60000)

        const registerRes = await AdminRequests.registerUser(user, true)
        expect(registerRes.statusCode).toEqual(200)

        const userRes = await AdminRequests.getUserByEmail(user.email)
        usernameId = jp.query(userRes.body, '$.content[0].tenantId')[0]

        authData = await UserRequests.getAuthData(user.email, user.password)
        await AdminRequests.changeBillingPlan(usernameId, '$79', true)
    })

    afterAll(async () => {
        await AdminRequests.deleteUser(usernameId)
    })

    afterEach(async () => {
        const date = await getZonedDate(30)
        await AdminRequests.changeBillingDate(usernameId, date)

        await AdminRequests.changeBillingPlan(usernameId, '$79', true)
    })

    it('Plan upgrade', async () => {
        await AdminRequests.changeBillingPlan(usernameId, '$229', false)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="SUBSCRIPTION_PLAN_UPGRADE")].tenantId')[0]).toEqual(usernameId)
        }).Run()
    })

    it('Plan downupgrade', async () => {
        await AdminRequests.changeBillingPlan(usernameId, '$49', false)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="SUBSCRIPTION_PLAN_DOWNGRADE")].tenantId')[0]).toEqual(
                usernameId,
            )
        }).Run()
    })

    it('Plan cancelled', async () => {
        await AdminRequests.changeBillingPlan(usernameId, '$49', false)
        const cancelSubscriptionResp = await UserRequests.cancelSubscription(authData)
        expect(cancelSubscriptionResp.status).toEqual(202)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="SUBSCRIPTION_CANCELLED")].tenantId')[0]).toEqual(usernameId)
        }).Run()
    })

    it('Pause Unpause subscription', async () => {
        const pauseResp = await UserRequests.pauseSubscription(authData, '1')
        expect(pauseResp.status).toEqual(202)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="SUBSCRIPTION_PAUSED")].tenantId')[0]).toEqual(usernameId)
        }).Run()

        const unPauseResp = await UserRequests.unpauseSubscription(authData)
        expect(unPauseResp.status).toEqual(202)

        await new WaitForResult(async () => {
            const getAnalyticResp = await AnalyticRequests.tenantHapsGet(usernameId, 7)
            expect(getAnalyticResp.statusCode).toEqual(200)

            const body = getAnalyticResp.body
            expect(jp.query(body, '$.content[?(@.type=="SUBSCRIPTION_UNPAUSED")].tenantId')[0]).toEqual(usernameId)
        }).Run()
    })
})
