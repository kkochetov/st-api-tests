import { newRegistrationUser } from '../../data/users.data'
import { Constants } from '../../data/constants'
import jp from 'jsonpath'
import WaitForResult from '../../core/waitForResult'
import { AdminRequests } from '../../requests/adminRequests'
import { AnalyticRequests } from '../../requests/analyticRequests'
import { UserRequests } from '../../requests/userRequests'

describe('Sign Up New User', () => {
    let usernameId: string
    const user = newRegistrationUser.ALL_LISTS
    const discount = false
    let authData: ITokens
    const phone = '4984949821'
    const code = '251216'

    beforeAll(async () => {
        jest.setTimeout(20000)

        const signUpResp = await UserRequests.signUpUser(user.email, user.password, discount, Constants.TEST_TOKEN)
        expect(signUpResp.statusCode).toEqual(200)
        usernameId = signUpResp.body.username

        authData = await UserRequests.getAuthData(user.email, user.password)
        console.log(`New User Id: ${usernameId}`)
    })

    afterAll(async () => {
        await AdminRequests.deleteUser(usernameId)
    })

    it('SignUp Method', async () => {
        await new WaitForResult(async () => {
            const snapshotAnalyticResp = await AnalyticRequests.tenantSnapshotGet(usernameId)
            expect(snapshotAnalyticResp.statusCode).toEqual(200)

            const body = snapshotAnalyticResp.body
            expect(jp.query(body, '$.content[0].tenantId')[0]).toEqual(usernameId)
            expect(jp.query(body, '$.content[0].values.signUpMethod')[0]).toEqual('standard')
            expect(jp.query(body, '$.content[0].values.registrationStatus')[0]).toEqual('NEW_ACCOUNT_CREATED')
            expect(jp.query(body, '$.content[0].values.email')[0]).toEqual(user.email)
        }).Run()
    })

    it('Sign up - Owner email verified', async () => {
        const userData: UserData = {
            fullName: 'Default Name',
            phone,
            country: 'US',
            websiteUrl: 'default.com',
            businessName: 'Analytics',
        }

        const registerProfileResp = await UserRequests.registerProfile(authData, userData)

        expect(registerProfileResp.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const snapshotAnalyticResp = await AnalyticRequests.tenantSnapshotGet(usernameId)
            expect(snapshotAnalyticResp.statusCode).toEqual(200)

            const body = snapshotAnalyticResp.body
            expect(jp.query(body, '$.content').length).toEqual(1)
            expect(jp.query(body, '$.content[0].tenantId')[0]).toEqual(usernameId)
            expect(jp.query(body, '$.content[0].values.email')[0]).toEqual(user.email)
        }).Run()
    })

    it('Owner phone verified', async () => {
        const verifyConfirmationCodeResp = await UserRequests.verifyConfirmationCode(authData, code)
        expect(verifyConfirmationCodeResp.statusCode).toEqual(200)

        await new WaitForResult(async () => {
            const snapshotAnalyticResp = await AnalyticRequests.tenantSnapshotGet(usernameId)
            expect(snapshotAnalyticResp.statusCode).toEqual(200)

            const body = snapshotAnalyticResp.body
            expect(jp.query(body, '$.content').length).toEqual(1)
            expect(jp.query(body, '$.content[0].values.registrationStatus')[0]).toEqual('PHONE_VERIFIED')
        }).Run()
    })
})
