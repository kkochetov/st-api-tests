export default class WaitForResult {
    private timeout: number // ms
    private callback: () => void

    private STEP_TIME = 500 // ms

    constructor(callback: () => void, timeout = 3000) {
        this.callback = callback
        this.timeout = timeout
    }

    public async Run() {
        let currentTime = 0
        let lastError

        while (currentTime < this.timeout) {
            try {
                await this.callback()
            } catch (err) {
                lastError = err
                await new Promise((r) => setTimeout(r, this.STEP_TIME))
                currentTime += this.STEP_TIME
                continue
            }
            return
        }

        console.log('Timeout is reached!')
        throw lastError
    }
}
