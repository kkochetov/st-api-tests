import { env } from 'process'

export const BASE_URL = env.BASE_URL || 'https://rt.st-ci.net'
export const ANALYTICS_URL = env.ANALYTICS_URL || 'https://analytics.st-ci.net/analytics/'
