import { UserType } from '../types/userTypes'

const SMPPCredentials: IUser = {
    email: 'tests',
    password: 'GpTFiDFz2jCR',
}

const GMAILCredentials: IUser = {
    email: 'qa-auth@simpletexting.net',
    password: 'simple1337',
}

const FBCredentials: IUser = {
    email: 'social_test@qualitas.su',
    password: 'simple1337',
}

const paypalCredentials: IUser = {
    email: 'paypal_test@qualitas.su',
    password: 'simple1337',
}

const enkioCredentials: IUser = {
    email: 'admin@inbox.st-ci.net',
    password: '0PxIWTzBr5h3',
}

export const newRegistrationUser: {
    [key in keyof typeof UserType]: IRegistrationUser
} = {
    ALL_LISTS: {
        fullName: 'Unverified phone',
        firstName: 'Unverified',
        lastName: 'phone',
        email: 'st_analytics_user_fields_ts_ts@inbox.st-ci.net',
        password: '12345678901234',
        phone: '3213211120',
        businessName: 'Unverified',
        websiteUrl: 'web.com',
        country: 'US',
    },
    ACCOUNT_ANALYTICS: {
        firstName: 'Default',
        lastName: 'Name',
        email: 'analytics_test_user3_ts@w.com',
        password: 'A1ptktyfzxthtgff',
        passwordAgain: 'A1ptktyfzxthtgff',
        zip: '',
        city: '',
        state: '',
        country: 'US',
        businessName: 'Analytics',
        street1: '',
        street2: '',
        phone: '4984949821',
    },
    ACCOUNT_PROFILE_ANALYTICS: {
        firstName: 'Default',
        lastName: 'Name',
        email: 'analytics_test_user4_ts@w.com',
        password: 'A1ptktyfzxthtgff',
        passwordAgain: 'A1ptktyfzxthtgff',
        zip: '',
        city: '',
        state: '',
        country: 'US',
        businessName: 'Analytics',
        street1: '',
        street2: '',
        phone: '4984949821',
    },
    BILLING_PAID_ANALYTICS: {
        firstName: 'Default',
        lastName: 'Name',
        email: 'analytics_test_user5_ts@w.com',
        password: 'A1ptktyfzxthtgff',
        passwordAgain: 'A1ptktyfzxthtgff',
        zip: '',
        city: '',
        state: '',
        country: 'US',
        businessName: 'Analytics',
        street1: '',
        street2: '',
        phone: '4984949821',
    },
}

const CREDENTIALS = (userName: keyof typeof UserType): IUser => ({
    email: newRegistrationUser[userName].email,
    password: newRegistrationUser[userName].password,
})

export const SYS_CREDENTIALS: { [key: string]: IUser } = {
    SMPP: SMPPCredentials,
    GMAIL: GMAILCredentials,
    FB: FBCredentials,
    PAYPAL: paypalCredentials,
    ROUNDCUBE: enkioCredentials,
}

export default CREDENTIALS
