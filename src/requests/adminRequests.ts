import { makeAdminRequest } from './requests'
import jp from 'jsonpath'

export class AdminRequests {
    static async deleteUser(userId: string) {
        await makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${userId}`,
            type: 'delete',
            parameter: 'skip-email=false',
        })
    }

    static async registerUser(userData: any, recreateIfExists = false) {
        if (recreateIfExists) {
            const userResp = await AdminRequests.getUserByEmail(userData.email)
            const userId = jp.query(userResp.body, '$.content[0].tenantId')[0]
            if (userId !== undefined) {
                await AdminRequests.deleteUser(userId)
            }
        }

        const res = makeAdminRequest({
            restModule: '/admin-rest/api/accounts',
            type: 'post',
            payload: userData,
        })
        return res
    }

    static async getUserByEmail(email: string) {
        const res = makeAdminRequest({
            restModule: `/admin-rest/api/accounts?emailFilter=${email}&`,
            type: 'get',
        })
        return res
    }

    static async banAccount(userId: string) {
        const res = makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${userId}/ban-account-action?`,
            type: 'put',
        })
        return res
    }

    static async unbanAccount(userId: string) {
        const res = makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${userId}/unban-account-action?`,
            type: 'put',
        })
        return res
    }

    static async getAvaiblePlans(usernameId: string) {
        const res = makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${usernameId}/billing-plans/available-packages?page=0&size=1000&`,
            type: 'get',
        })
        return res
    }

    static async changeBillingPlan(usernameId: string, planName: string, free: boolean) {
        const avaiblePlansResp = await AdminRequests.getAvaiblePlans(usernameId)
        expect(avaiblePlansResp.status).toEqual(200)
        const aa = jp.query(avaiblePlansResp.body, `$.content[?(/\\${planName}/.test(@.name))].id`)[0]

        const res = await makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${usernameId}/billing-plans/change-billing-plan-actions?annual=false&`,
            type: 'post',
            payload: { newPlanId: aa },
        })
        expect(res.status).toEqual(200)

        const res1 = await makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${usernameId}/billing-plans/update-free-for-life-flag-actions?`,
            type: 'post',
            payload: { freeForLife: free },
        })
        expect(res1.status).toEqual(200)
    }

    static async changeBillingDate(usernameId: string, date: string) {
        const res = await makeAdminRequest({
            restModule: `/admin-rest/api/accounts/${usernameId}/update-next-billing-cycle-time/${date}`,
            type: 'put',
        })
        expect(res.status).toEqual(200)
    }
}
