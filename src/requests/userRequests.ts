import { Constants } from '../data/constants'
import { makeRestRequest } from './requests'

export class UserRequests {
    static signUpUser = async (email: string, password: string, discount: boolean, recapcha: string) => {
        const jsonBody = {
            email,
            password,
            discount,
            recaptchaToken: recapcha,
        }
        const res = await makeRestRequest({
            restModule: 'sign_up',
            type: 'post',
            payload: jsonBody,
        })
        return res
    }

    static registerProfile = async (authData: any, userData: UserData) => {
        const res = await makeRestRequest({
            restModule: 'profile',
            type: 'post',
            payload: userData,
            tokensData: authData,
        })
        return res
    }

    static verifyConfirmationCode = async (authData: any, smsCode: string) => {
        const res = await makeRestRequest({
            restModule: 'confirm/sms/verify-confirmation-code',
            type: 'post',
            payload: { code: smsCode },
            tokensData: authData,
        })
        return res
    }

    static updatePhoneNumber = async (authData: any, newPhoneNumber: string) => {
        const res = await makeRestRequest({
            restModule: `profile/text-enable/?phone=${newPhoneNumber}`,
            type: 'put',
            cookies: [`remember-me=${authData.remember}`],
        })
        return res
    }

    static updateProfileInfo = async (authData: any, profileInfo: any) => {
        const res = await makeRestRequest({
            restModule: 'profile',
            type: 'put',
            cookies: [`remember-me=${authData.remember}`],
            payload: profileInfo,
        })

        return res
    }

    static getLoginToken = async (username: string, password: string) => {
        const payload = {
            username,
            password,
            'remember-me': 'on',
            recaptchaToken: Constants.TEST_TOKEN,
        }

        const res = makeRestRequest({
            restModule: 'auth/login',
            type: 'post',
            payload,
        })

        return res
    }

    static cancelSubscription = async (authData: any) => {
        const res = await makeRestRequest({
            restModule: 'billing/cancel-subscription-action?cancelReason=',
            type: 'put',
            cookies: [`remember-me=${authData.remember}`],
        })

        return res
    }

    static pauseSubscription = async (authData: any, month: string) => {
        const res = await makeRestRequest({
            restModule: `billing/pause-subscription?pauseDurationInMonths=${month}`,
            type: 'put',
            cookies: [`remember-me=${authData.remember}`],
        })
        return res
    }

    static unpauseSubscription = async (authData: any) => {
        const res = makeRestRequest({
            restModule: 'billing/unpause-subscription',
            type: 'put',
            cookies: [`remember-me=${authData.remember}`],
        })
        return res
    }

    static getCsrfToken = async (rememberToken: string) => {
        const res = await makeRestRequest({
            restModule: 'csrf-token',
            type: 'put',
            cookies: [`remember-me=${rememberToken}`],
        })
        return res
    }

    static getAuthData = async (email: string, password: string): Promise<ITokens> => {
        const loginRes = await UserRequests.getLoginToken(email, password)
        expect(loginRes.statusCode).toEqual(200)
        const rememberToken = loginRes.body.token

        const tokenResp = await UserRequests.getCsrfToken(rememberToken)
        expect(tokenResp.statusCode).toEqual(200)
        const csfToken = tokenResp.body.token
        return { csrf: csfToken, remember: rememberToken }
    }
}
