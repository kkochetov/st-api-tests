import { ANALYTICS_URL, BASE_URL } from '../config/globalConfig'
import request from 'supertest'
import { getAdminTokens } from '../helpers/adminToken'
import { analyzeAndFormatAuthData } from '../helpers/cookiesHandler'

type RequestData = {
    restModule: string
    type: 'post' | 'get' | 'delete' | 'put'
    tokensData?: ITokens
    cookies?: string[]
    country?: 'us' | 'ca'
    parameter?: string
    payload?: any
    addHeaders?: any
    fullResponse?: boolean
    isContent?: boolean
    bearerToken?: string
}

export const makeRestRequest = (reqData: RequestData) => {
    switch (reqData.type) {
        case 'post':
            return makePostRequest(reqData)
        case 'put':
            return makePutRequest(reqData)
        case 'delete':
            return makeDeleteRequest(reqData)
        case 'get':
        default:
            return makeGetRequest(reqData)
    }
}

const makeGetRequest = (reqData: RequestData) => {
    const restUrl = `${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`
    const res = request(BASE_URL)
        .get(restUrl)
        .set('Authorization', 'Bearer ' + reqData.bearerToken)
    return res
}

const makePostRequest = (reqData: RequestData) => {
    const requestStart = reqData.isContent ? 'content' : 'rest'
    const restUrl = `/${requestStart}/${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`
    const formatedAuthData = analyzeAndFormatAuthData(reqData.cookies, reqData.tokensData, reqData.addHeaders, false)
    const res = request(BASE_URL)
        .post(restUrl)
        .set('Cookie', formatedAuthData.finalCookies as string[])
        .set(formatedAuthData.finalHeaders)
        .send(reqData.payload)
    return res
}

const makeDeleteRequest = (reqData: RequestData) => {
    throw new Error('Function not implemented.')
}

const makePutRequest = (reqData: RequestData) => {
    const requestStart = reqData.isContent ? 'content' : 'rest'
    const restUrl = `/${requestStart}/${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`
    const res = request(BASE_URL).put(restUrl).set('Cookie', reqData.cookies!).send(reqData.payload)
    return res
}

export const makeAdminRequest = (reqData: RequestData) => {
    switch (reqData.type) {
        case 'post':
            return makeAdminPostRequest(reqData)
        case 'put':
            return makeAdminPutRequest(reqData)
        case 'delete':
            return makeAdminDeleteRequest(reqData)
        case 'get':
        default:
            return makeAdminGetRequest(reqData)
    }
}

export const makeAdminGetRequest = async (reqData: RequestData) => {
    const adminAuth = await getAdminTokens()
    const formatedAuthData = analyzeAndFormatAuthData(reqData.cookies, adminAuth, reqData.addHeaders, true)

    const res = request(BASE_URL)
        .get(`${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`)
        .set('Cookie', formatedAuthData.finalCookies as string[])
        .set(formatedAuthData.finalHeaders)
    return res
}

export const makeAdminPostRequest = async (reqData: RequestData) => {
    const adminAuth = await getAdminTokens()
    const formatedAuthData = analyzeAndFormatAuthData(reqData.cookies, adminAuth, reqData.addHeaders, true)
    const res = request(BASE_URL)
        .post(`${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`)
        .set('Cookie', formatedAuthData.finalCookies as string[])
        .set(formatedAuthData.finalHeaders)
        .send(reqData.payload)
    return res
}

export const makeAdminPutRequest = async (reqData: RequestData) => {
    const adminAuth = await getAdminTokens()
    const formatedAuthData = analyzeAndFormatAuthData(reqData.cookies, adminAuth, reqData.addHeaders, true)

    const res = request(BASE_URL)
        .put(`${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`)
        .set('Cookie', formatedAuthData.finalCookies as string[])
        .set(formatedAuthData.finalHeaders)
    return res
}

export const makeAdminDeleteRequest = async (reqData: RequestData) => {
    const adminAuth = await getAdminTokens()
    const formatedAuthData = analyzeAndFormatAuthData(reqData.cookies, adminAuth, reqData.addHeaders, true)

    const res = request(BASE_URL)
        .delete(`${reqData.restModule}${reqData.parameter ? '?' + reqData.parameter : ''}`)
        .set('Cookie', formatedAuthData.finalCookies as string[])
        .set(formatedAuthData.finalHeaders)
    return res
}

export const makeAnalyticsGetRequest = async (url: string) => {
    const res = await request(ANALYTICS_URL).get(url)
    return res
}
