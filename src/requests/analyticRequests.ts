import { getZonedTime } from '../helpers/dateHelpers'
import { makeAnalyticsGetRequest, makeRestRequest } from './requests'

export class AnalyticRequests {
    static sendAnalyticsField = async (authData: any, fieldType: string, analyticsInfo: any) => {
        const res = await makeRestRequest({
            restModule: `signup-haps/${fieldType}`,
            type: 'post',
            payload: analyticsInfo,
            tokensData: authData,
        })
        return res
    }

    static tenantSnapshotGet = async (tenantId: string) => {
        const res = makeAnalyticsGetRequest(`tenant-snapshots/?tenantId=${tenantId}`)
        return res
    }

    static tenantHapsGet = async (tenantId: string, secondsBefore: number) => {
        const zonedTime = await getZonedTime(-secondsBefore)
        const res = makeAnalyticsGetRequest(`haps/?tenantId=${tenantId}&from=${zonedTime}`)
        return res
    }
}
